﻿<div class="form">
<h1>Đăng Ký Thành Viên Mới</h1>
<?php
	// mở thẻ form chuyển method = post, set foucs
	$form = $this->beginWidget('CActiveForm',array(
			'id'=>'frm-dangky',
			'method'=>'post',
			'focus'=>array($model,'thoten'),
			'enableClientValidation'=>true,
	));
?>
<div class="row">
	<?php
		echo $form->labelEx($model,'hoten');
		echo $form->textField($model,'hoten');
		echo $form->error($model,'hoten');
	?>
</div>

<div class="row">
	<?php
		echo $form->labelEx($model,'tendangnhap');
		echo $form->textField($model,'tendangnhap');
		echo $form->error($model,'tendangnhap');
	?>
</div>

<div class="row">
	<?php
		echo $form->labelEx($model,'dienthoai');
		echo $form->textField($model,'dienthoai');
		echo $form->error($model,'dienthoai');
	?>
</div>

<div class="row">
	<?php
		echo $form->labelEx($model,'hopthu');
		echo $form->textField($model,'hopthu');
		echo $form->error($model,'hopthu');
	?>
</div>

<div class="row">
	<?php
		echo $form->labelEx($model,'matkhau');
		echo $form->textField($model,'matkhau');
		echo $form->error($model,'matkhau');
	?>
</div>

<div class="row">
	<?php
		echo $form->labelEx($model,'nhaplaimatkhau');
		echo $form->textField($model,'nhaplaimatkhau');
		echo $form->error($model,'nhaplaimatkhau');
	?>
</div>
<?php $this->widget('CCaptcha'); ?>
<div class="row">
	<?php
		echo $form->labelEx($model,'maxacnhan');
		echo $form->textField($model,'maxacnhan');
		echo $form->error($model,'maxacnhan');
	?>
</div>
<div class="row radio">
	<?php
		echo $form->labelEx($model,'gioitinh');
		echo $form->radioButtonList($model,'gioitinh',array('0'=>'Nữ','1'=>'Nam'));
	?>
</div>

<div class="row">
	<?php echo CHtml::submitButton('Đăng Ký') ?>
</div>
<?php
	$this->endWidget();
?>
</div>