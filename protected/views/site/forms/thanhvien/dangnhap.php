﻿<div class="form">
	<h1>Đăng Nhập</h1>
	<?php
		$form = $this->beginWidget('CActiveForm',array(
			'id'=>'frm-dangnhap',
			'method'=>'post'
		));
	?>
	<div class="row">
		<?php
			echo $form->labelEx($model,'tendangnhap');
			echo $form->textField($model,'tendangnhap');
			echo $form->error($model,'tendangnhap');
		?>
	</div>
	<div class="row">
		<?php
			echo $form->labelEx($model,'matkhau');
			echo $form->textField($model,'matkhau');
			echo $form->error($model,'matkhau');
		?>
	</div>
	<div class="row">
		<?php
			echo $form->labelEx($model,'ghinho');
			echo $form->checkBox($model,'ghinho');
			echo $form->error($model,'ginho');
		?>
	</div>
	<div class="row button">
		<?php echo CHtml::submitButton("Đăng Nhập"); ?>
	</div>
	<?php
		$this->endWidget();
	?>
</div>