﻿<?php
/*
Author:longdhb
Email:kevindng@zing.vn
*/
class FrmDangKy extends CFormModel{
	public $hoten;
	public $tendangnhap;
	public $hopthu;
	public $matkhau;
	public $nhaplaimatkhau;
	public $maxacnhan;
	public $gioitinh=1;
	public $dienthoai;
	public function rules(){
		return array(
			array('hoten, tendangnhap, hopthu, matkhau, nhaplaimatkhau, maxacnhan, dienthoai','required'),
			array('tendangnhap, hopthu','unique','className'=>'Thanhvien'),
			array('tendangnhap','match','pattern'=>'/^[a-z0-9]+$/i'),
			array('dienthoai','match','pattern'=>'/^[+0-9]{8,20}/'),
			array('hopthu','email'),
			array('maxacnhan','captcha','allowEmpty'=>!CCaptcha::checkRequirements()),
			array('gioitinh','in','range'=>array(0,1)),
		);
	}
	
	public function attributeLabels(){
		return array(
			'hoten'=>'Họ Tên',
			'tendangnhap'=>'Tên Đăng Nhập',
			'hopthu'=>'Hộp Thư',
			'matkhau'=>'Mật Khẩu',
			'nhaplaimatkhau'=>'Nhập Lại Mật Khẩu',
			'maxacnhan'=>'Mã Xác Nhận',
			'gioitinh'=>'Giới Tính',
			'dienthoai'=>'Điện Thoại',
		);
	}
	public function save(){
		$thanhvien = new Thanhvien();
		$thanhvien->attributes = $this->attributes;
		return $thanhvien->save(false);
	}

}
?>