<?php

/**
 * This is the model class for table "sp_thanhvien".
 *
 * The followings are the available columns in table 'sp_thanhvien':
 * @property integer $thanhvienid
 * @property integer $hoten
 * @property string $tendangnhap
 * @property string $matkhau
 * @property string $maxacnhan
 * @property string $hopthu
 * @property integer $gioitinh
 * @property integer $kichhoat
 * @property string $ngaytao
 * @property integer $dienthoai
 */
class Thanhvien extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Thanhvien the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sp_thanhvien';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('hoten, tendangnhap, matkhau, maxacnhan, hopthu, ngaytao, dienthoai', 'required'),
			array('hoten, gioitinh, kichhoat, dienthoai', 'numerical', 'integerOnly'=>true),
			array('tendangnhap', 'length', 'max'=>20),
			array('matkhau, maxacnhan', 'length', 'max'=>32),
			array('hopthu', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('thanhvienid, hoten, tendangnhap, matkhau, maxacnhan, hopthu, gioitinh, kichhoat, ngaytao, dienthoai', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'thanhvienid' => 'Thanhvienid',
			'hoten' => 'Hoten',
			'tendangnhap' => 'Tendangnhap',
			'matkhau' => 'Matkhau',
			'maxacnhan' => 'Maxacnhan',
			'hopthu' => 'Hopthu',
			'gioitinh' => 'Gioitinh',
			'kichhoat' => 'Kichhoat',
			'ngaytao' => 'Ngaytao',
			'dienthoai' => 'Dienthoai',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('thanhvienid',$this->thanhvienid);
		$criteria->compare('hoten',$this->hoten);
		$criteria->compare('tendangnhap',$this->tendangnhap,true);
		$criteria->compare('matkhau',$this->matkhau,true);
		$criteria->compare('maxacnhan',$this->maxacnhan,true);
		$criteria->compare('hopthu',$this->hopthu,true);
		$criteria->compare('gioitinh',$this->gioitinh);
		$criteria->compare('kichhoat',$this->kichhoat);
		$criteria->compare('ngaytao',$this->ngaytao,true);
		$criteria->compare('dienthoai',$this->dienthoai);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	//over function
	protected function beforeSave(){
	
		if(parent::beforeSave()){
			if($this->isNewRecord){
				$this->maxacnhan = $this->makeRandom($this->maxacnhan);
				$this->matkhau  = $this->makePass($this->matkhau,$this->maxacnhan);
				$this->ngaytao = date('y-m-d',time());
			}
			return true;
		}else{
			return false;
		}
	}
	public function makePass($pass,$random){
		return md5($pass.$random);
	}
	public function makeRandom($key){
		return md5($key.Yii::app()->params['salt']);
	}
}